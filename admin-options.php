<?php
global $wpdb;

echo "<h2>Stats Plugin</h2>";

echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post">';
echo "<input type='text' name='stat'/>";
echo "<button type='submit'>Submit</button>";
echo "</form>";

if(isset($_POST['stat'])) {
    echo "We have added your great stat! Thank you for your generious contributions to our database.";
    $user_id = 1;
	
	$table_name = $wpdb->prefix . 'sp_stats';
	
	$wpdb->insert( 
		$table_name, 
		array( 
			'user_id' => $user_id, 
			'stat_text' => $_POST['stat']
		) 
	);
}
